package com.example.rabbit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Author:aijiaxiang
 * Date:2020/5/1
 * Description:
 */
@Controller
public class TestController {

    @RequestMapping("/test")
    public String test(){
        return "hello";
    }
}
