package com.example.rabbit.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Author:aijiaxiang
 * Date:2020/5/1
 * Description:
 */
@Controller
public class RabbitController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping(value="rabbit",method= RequestMethod.GET)
    public void rabbit(){


        //根据key发送到对应的队列
        rabbitTemplate.convertAndSend("queuekey", "hello");


        //根据key发送到对应的队列
        rabbitTemplate.convertAndSend("queuekey", "world");

    }

}

