package com.example.rabbit.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

/**
 * Author:aijiaxiang
 * Date:2020/5/1
 * Description:
 */
public class RecvHandler implements MessageListener {


    @Override
    public void onMessage(Message message) {

            // msg就是rabbitmq传来的消息，需要的同学自己打印看一眼
            // 使用jackson解析

        System.out.println(message);

    }


}
